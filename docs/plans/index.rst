============
Future plans
============

Various things I plan to add to Pimlico in the futures. For a summary, see :doc:`wishlist`.

.. toctree::
   :maxdepth: 2

   wishlist
   berkeley
   cherry_picker
   drawing
