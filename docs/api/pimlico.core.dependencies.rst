pimlico.core.dependencies package
=================================

Submodules
----------

.. toctree::

   pimlico.core.dependencies.base
   pimlico.core.dependencies.core
   pimlico.core.dependencies.java
   pimlico.core.dependencies.python

Module contents
---------------

.. automodule:: pimlico.core.dependencies
    :members:
    :undoc-members:
    :show-inheritance:
