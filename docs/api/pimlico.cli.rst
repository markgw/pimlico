pimlico.cli package
===================

Subpackages
-----------

.. toctree::

    pimlico.cli.browser
    pimlico.cli.shell

Submodules
----------

.. toctree::

   pimlico.cli.check
   pimlico.cli.run
   pimlico.cli.status

Module contents
---------------

.. automodule:: pimlico.cli
    :members:
    :undoc-members:
    :show-inheritance:
