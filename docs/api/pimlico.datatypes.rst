pimlico.datatypes package
=========================

Subpackages
-----------

.. toctree::

    pimlico.datatypes.coref
    pimlico.datatypes.parse

Submodules
----------

.. toctree::

   pimlico.datatypes.arrays
   pimlico.datatypes.base
   pimlico.datatypes.caevo
   pimlico.datatypes.dictionary
   pimlico.datatypes.features
   pimlico.datatypes.files
   pimlico.datatypes.jsondoc
   pimlico.datatypes.plotting
   pimlico.datatypes.results
   pimlico.datatypes.table
   pimlico.datatypes.tar
   pimlico.datatypes.tokenized
   pimlico.datatypes.word2vec
   pimlico.datatypes.word_annotations
   pimlico.datatypes.xml

Module contents
---------------

.. automodule:: pimlico.datatypes
    :members:
    :undoc-members:
    :show-inheritance:
