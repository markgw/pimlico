pimlico.datatypes.word2vec module
=================================

.. automodule:: pimlico.datatypes.word2vec
    :members:
    :undoc-members:
    :show-inheritance:
