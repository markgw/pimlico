pimlico.utils.format module
===========================

.. automodule:: pimlico.utils.format
    :members:
    :undoc-members:
    :show-inheritance:
