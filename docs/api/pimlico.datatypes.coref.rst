pimlico.datatypes.coref package
===============================

Submodules
----------

.. toctree::

   pimlico.datatypes.coref.corenlp
   pimlico.datatypes.coref.opennlp

Module contents
---------------

.. automodule:: pimlico.datatypes.coref
    :members:
    :undoc-members:
    :show-inheritance:
