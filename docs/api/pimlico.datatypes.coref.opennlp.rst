pimlico.datatypes.coref.opennlp module
======================================

.. automodule:: pimlico.datatypes.coref.opennlp
    :members:
    :undoc-members:
    :show-inheritance:
