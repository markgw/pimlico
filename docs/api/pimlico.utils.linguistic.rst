pimlico.utils.linguistic module
===============================

.. automodule:: pimlico.utils.linguistic
    :members:
    :undoc-members:
    :show-inheritance:
