Regular expressions
~~~~~~~~~~~~~~~~~~~


.. py:module:: pimlico.modules.regex



.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.regex.annotated_text
