Visualization tools
~~~~~~~~~~~~~~~~~~~


.. py:module:: pimlico.modules.visualization


Modules for plotting and suchlike



.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.visualization.bar_chart
