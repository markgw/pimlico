Feature set processing
~~~~~~~~~~~~~~~~~~~~~~


.. py:module:: pimlico.modules.features


Various tools for generic processing of extracted sets of features: building vocabularies, mapping to integer
indices, etc.


.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.features.term_feature_compiler
   pimlico.modules.features.term_feature_matrix_builder
   pimlico.modules.features.vocab_builder
   pimlico.modules.features.vocab_mapper
