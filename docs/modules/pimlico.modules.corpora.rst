Corpus-reading
~~~~~~~~~~~~~~


.. py:module:: pimlico.modules.corpora


Base modules for reading input from textual corpora.



.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.corpora.format
   pimlico.modules.corpora.split
   pimlico.modules.corpora.subset
   pimlico.modules.corpora.tar
   pimlico.modules.corpora.tar_filter
   pimlico.modules.corpora.vocab_builder
