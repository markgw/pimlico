Core Pimlico modules
~~~~~~~~~~~~~~~~~~~~


.. py:module:: pimlico.modules


Pimlico comes with a substantial collection of module types that provide wrappers around existing NLP and
machine learning tools.


.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.caevo
   pimlico.modules.candc
   pimlico.modules.corenlp
   pimlico.modules.corpora
   pimlico.modules.embeddings
   pimlico.modules.features
   pimlico.modules.malt
   pimlico.modules.opennlp
   pimlico.modules.regex
   pimlico.modules.sklearn
   pimlico.modules.utility
   pimlico.modules.visualization
