Malt dependency parser
~~~~~~~~~~~~~~~~~~~~~~


.. py:module:: pimlico.modules.malt


Wrapper around the `Malt dependency parser <http://www.maltparser.org/>`_
and data format converters to support connections to other modules.


.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.malt.conll_parser_input
   pimlico.modules.malt.parse
