Key-value to term-feature converter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. py:module:: pimlico.modules.features.term_feature_compiler

+------------+------------------------------------------------+
| Path       | pimlico.modules.features.term_feature_compiler |
+------------+------------------------------------------------+
| Executable | yes                                            |
+------------+------------------------------------------------+

.. todo::

   Document this module


Inputs
======

+------------+-----------------------------------------------------------------------------+
| Name       | Type(s)                                                                     |
+============+=============================================================================+
| key_values | :class:`KeyValueListCorpus <pimlico.datatypes.features.KeyValueListCorpus>` |
+------------+-----------------------------------------------------------------------------+

Outputs
=======

+---------------+------------------------------------------------------------+
| Name          | Type(s)                                                    |
+===============+============================================================+
| term_features | :class:`~pimlico.datatypes.features.TermFeatureListCorpus` |
+---------------+------------------------------------------------------------+

Options
=======

+----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------+
| Name                 | Description                                                                                                                                                                                                                                                                                              | Type                           |
+======================+==========================================================================================================================================================================================================================================================================================================+================================+
| term_keys            | Name of keys (feature names in the input) which denote terms. The first one found in the keys of a particular data point will be used as the term for that data point. Any other matches will be removed before using the remaining keys as the data point's features. Default: just 'term'              | comma-separated list of string |
+----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------+
| include_feature_keys | If True, include the key together with the value from the input key-value pairs as feature names in the output. Otherwise, just use the value. E.g. for input [prop=wordy, poss=my], if True we get features [prop_wordy, poss_my] (both with count 1); if False we get just [wordy, my]. Default: False | bool                           |
+----------------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------------------------------+

