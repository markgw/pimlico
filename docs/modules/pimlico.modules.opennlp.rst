OpenNLP modules
~~~~~~~~~~~~~~~


.. py:module:: pimlico.modules.opennlp


A collection of module types to wrap individual OpenNLP tools.



.. toctree::
   :maxdepth: 2
   :titlesonly:

   pimlico.modules.opennlp.coreference
   pimlico.modules.opennlp.coreference_pipeline
   pimlico.modules.opennlp.parse
   pimlico.modules.opennlp.pos
   pimlico.modules.opennlp.tokenize
