===================
Downloading Pimlico
===================

Pimlico is available for download from `its Gitlab page <https://gitlab.com/markgw/pimlico>`_.

If you're starting a new project using Pimlico, the best way is to use the `newproject.py` script, which will
create a template pipeline config file to get you started and download the latest version of Pimlico to accompany
it.

Manual setup
============
If for some reason you don't want to use the `newproject.py` script, you can set up a project yourself.
You'll want to download either
`the latest release <https://gitlab.com/markgw/pimlico/tags>`_ or the bleeding edge version,
`from the homepage <https://gitlab.com/markgw/pimlico>`_ (which might be a bit less stable).

Simply download the whole source code as a .zip or .tar.gz file and uncompress it. This will produce a directory
called `pimlico`, followed by a long incomprehensible string, which you can rename simply `pimlico`.

Pimlico has a few basic dependencies, but these will be automatically downloaded the first time you load it.

See :doc:`/guides/setup` for more on getting started with Pimlico.
