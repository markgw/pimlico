===============
Pipeline config
===============

.. currentmodule:: pimlico.core.config

A Pimlico pipeline, as read from a config file (:class:`pimlico.core.config.PipelineConfig`) contains all the information about the
pipeline being processed and provides access to specific modules in it.

.. todo::

   Write full documentation for this
