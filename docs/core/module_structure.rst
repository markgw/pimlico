========================
Pimlico module structure
========================

This document describes the code structure for Pimlico module types in full.

For a basic guide to writing your own modules, see :doc:`/guides/module`.

.. todo::

   Write documentation for this
