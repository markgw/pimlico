===================
Module dependencies
===================

.. todo::

   Write something about how dependencies are fetched

.. note::

   Pimlico now has a really neat way of checking for dependencies and, in many cases, fetching the automatically.
   It's rather new, so I've not written this guide yet. Ignore any old Makefiles: they ought to have all been
   replaced by SoftwareDependency classes now
