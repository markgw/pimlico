==============
Pimlico guides
==============

Step-by-step guides through common tasks while using Pimlico.

.. toctree::
   :maxdepth: 2

   setup
   module
   map_module

