"""
Visualization tools

Modules for plotting and suchlike

"""
from pimlico.core.dependencies.python import PythonPackageOnPip

matplotlib_dependency = PythonPackageOnPip("matplotlib")
