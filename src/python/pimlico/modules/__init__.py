"""Core Pimlico modules

Pimlico comes with a substantial collection of module types that provide wrappers around existing NLP and
machine learning tools.
"""