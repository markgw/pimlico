__author__ = 'mark'


class CoreNLPClientError(Exception):
    pass


class CoreNLPProcessingError(Exception):
    pass