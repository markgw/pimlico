"""
Malt dependency parser

Wrapper around the `Malt dependency parser <http://www.maltparser.org/>`_
and data format converters to support connections to other modules.
"""